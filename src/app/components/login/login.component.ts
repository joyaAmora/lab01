import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  invalidInfo = '';
  loginForm: FormGroup;

  constructor(private authService: AuthService, private router: Router, private translateService: TranslateService) {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(3), this.passwordMaxLength])
    });
  }

  private passwordMaxLength(control: AbstractControl): ValidationErrors | null {
    if (control.value.length >6) {
      return { maxLengthPassword: true };
    } else {
      return null
    }
  }
  ngOnInit(): void {

  }



  logIn() {
    this.invalidInfo = this.translateService.instant('invalidCredentials');
    this.authService.logIn(this.loginForm.value).subscribe(success => {
      if (success) {
        this.router.navigate(['/recipes']);
      } else {
        alert(this.invalidInfo + "!");
      }
    });
  }
}
