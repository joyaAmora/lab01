import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserCredentials } from 'src/app/models/user-credentials.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signUpForm: FormGroup;

  constructor(private authService: AuthService, private router: Router) {
    this.signUpForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(3)]),
      passwordConfirmation: new FormControl()
    }, this.passwordMatch);
  }

  private passwordMatch(form: AbstractControl): ValidationErrors | null {
    if (form.value?.password != form.value?.passwordConfirmation) {
      return { passwordConfirmationMustMatch: true };
    } else {
      return null
    }
  }

  ngOnInit(): void {
  }

  submit() {
    this.authService.signUp(new UserCredentials(this.signUpForm.value)).subscribe(success => {
      console.log('Sign up component', success);

      if (success)
        this.router.navigate(['/recipes']);
    else
      alert('email already exist');
    });
  }
}
