import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Recipe } from 'src/app/models/recipe.model';
import { AuthService } from 'src/app/services/auth.service';
import { RecipeService } from 'src/app/services/recipe.service';

@Component({
  selector: 'app-add-recipe',
  templateUrl: './add-recipe.component.html',
  styleUrls: ['./add-recipe.component.css'],
})
export class AddRecipeComponent implements OnInit {
  addRecipeForm: FormGroup;
  recipeList  : Recipe[] | null = null;

  constructor(private recipeService: RecipeService, private router: Router, private authService : AuthService, private route: ActivatedRoute) {
    this.addRecipeForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      description: new FormControl('', [
        Validators.required,
        Validators.minLength(5),
      ]),
      category: new FormControl(1),
      user_email: new FormControl(this.authService.currentUser?.email),
    });
  }

  ngOnInit(): void {
    let recipeId = this.route.snapshot.paramMap.get('id');
    if(recipeId) {
      this.recipeService.getRecipe(recipeId, this.authService.currentUser!).subscribe(
        (recipes) => {
          this.recipeList = recipes;
          this.editRecipe(this.recipeList![0]);
        }
      );

    }
  }

  editRecipe(recipe: Recipe) {
    this.addRecipeForm.patchValue({
      name: recipe.name,
      description: recipe.description,
      category: recipe.category,
      user_email: recipe.user_email
    });
  }

  submit() {
    if(this.route.snapshot.paramMap.get('id')) {
      let editRecipe = new Recipe({
        id: this.route.snapshot.paramMap.get('id'),
        category: parseInt(this.addRecipeForm.controls.category.value),
        name: this.addRecipeForm.controls.name.value,
        description: this.addRecipeForm.controls.description.value,
        user_email: this.authService.currentUser?.email,
      });

      if (this.recipeService.editRecipe(new Recipe(editRecipe)).subscribe())
        this.router.navigate(['recipes']);
    }
    else {
      let newRecipe = new Recipe({
        id: Math.random().toString(16).substring(2),
        category: parseInt(this.addRecipeForm.controls.category.value),
        name: this.addRecipeForm.controls.name.value,
        description: this.addRecipeForm.controls.description.value,
        user_email: this.authService.currentUser?.email,
      });

      if (this.recipeService.addRecipe(new Recipe(newRecipe)).subscribe())
        this.router.navigate(['recipes']);
    }

  }
}
