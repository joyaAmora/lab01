import { Component, OnInit } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { MarthaRequestService } from 'src/app/services/martha-request.service';
import { RecipeService } from 'src/app/services/recipe.service';

@Component({
  selector: 'app-recipes-list',
  templateUrl: './recipes-list.component.html',
  styleUrls: ['./recipes-list.component.css']
})
export class RecipesListComponent implements OnInit {
  recipeList: Recipe[] = [];
  user: User;

  get recipes(): Recipe[] {
    return this.recipeList;
  }

  constructor(private recipeService: RecipeService, private authService: AuthService, private martha: MarthaRequestService) {
      this.user = authService.currentUser!;
      this.recipeService.getAllRecipes(this.user).subscribe(data => {
        if(data)
          this.recipeList = data.map((r: any) => new Recipe(r));
      });
  }

  ngOnInit(): void {

  }

  deleteRecipe(recipeToDelete : Recipe) {
    this.recipeList = this.recipeList.filter(r => r.id !== recipeToDelete.id);
  }

}
