import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { routes } from 'src/app/app.routes';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from 'src/app/services/recipe.service';

@Component({
  selector: 'app-recipes-item',
  templateUrl: './recipes-item.component.html',
  styleUrls: ['./recipes-item.component.css']
})
export class RecipesItemComponent implements OnInit {

  @Input() recipe!: Recipe;
  @Output() deleteRecipe = new EventEmitter();

  constructor(private recipeService: RecipeService) { }

  ngOnInit(): void {
  }

  get categoryName(): string {
    switch (this.recipe.category) {
      case 1 :
        return "Breakfast";
      case 2 :
        return "Dinner";
      case 3 :
        return "Lunch";
      default :
        return "Unknown";
    }
  }
  deleteItem() {
    if(confirm("Are you sure you want to delete " + this.recipe.name + "?")){
      this.recipeService.delete(this.recipe);
      this.deleteRecipe.emit(this.recipe);
    }
  }
}
