import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { MarthaRequestService } from 'src/app/services/martha-request.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  users: User[] | null = null;


  constructor(private authService: AuthService, private router: Router, private martha : MarthaRequestService) {
    this.martha.select('users-list').subscribe(data => {
      console.log(data);
      if(data)
        this.users = data.map(d => new User(d));
    });
   }

  ngOnInit(): void {
  }

  userClicked(userEmail: string) {
    this.router.navigate(['/profile', userEmail]);
  }

}
