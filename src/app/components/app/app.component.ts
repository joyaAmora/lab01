import { transformAll } from '@angular/compiler/src/render3/r3_ast';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/services/auth.service';
import { RecipeService } from 'src/app/services/recipe.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  storedValue: string = '';
  selectedLang: string = '';

  constructor(private authService: AuthService, private recipeService: RecipeService, private router: Router, private translate: TranslateService) {
    translate.setDefaultLang('en');
    translate.addLangs(['en', 'fr']);
    if(localStorage.length == 0){
      this.translate.use('en');
    }
    else{
      this.storedValue = localStorage.getItem("language")!;
      this.translate.use(this.storedValue);
    }

  }

  ngOnInit(): void {

  }

  logOut() {
    this.authService.logOut();

    this.router.navigate(['/']);
  }

  changeLang(event: any) {
    this.selectedLang = event.target.value;
    this.translate.use(this.selectedLang);
    localStorage.setItem("language",this.selectedLang);
  }


  get isLoggedIn() :boolean {
    if(this.authService.isLoggedIn)
      return true;
    else
      return false;
  }
}
