import { custom, JSONObject, optional, required } from "ts-json-object";

export class Recipe extends JSONObject {
  @required
  id!: string;
  @required
  category!: number;
  @required
  name!: string;
  @optional
  description!: string;
  @optional
  user_email!: string;
}
