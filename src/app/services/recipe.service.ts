import { createElementCssSelector } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { routes } from '../app.routes';
import { Recipe } from '../models/recipe.model';
import { User } from '../models/user.model';
import { AuthService } from './auth.service';
import { MarthaRequestService } from './martha-request.service';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  allRecipes : Recipe[] | null = null;

  private readonly RECIPES_KEY = "recipeasy.recipes";

  constructor(private martha: MarthaRequestService, private authService: AuthService) { }

  getAllRecipes(currentUser: User): Observable<Recipe[]>{
    return this.martha.select('recipes-list', currentUser).pipe(
      map(data => {
        if(data)
          this.allRecipes = data.map((r: any) => new Recipe(r));
        return this.allRecipes!;
      })
    );
  }

  getRecipe(id: string, currentUser :User): Observable<Recipe[] | null>{
    return this.martha.select('recipes-read', {id: id, email: currentUser.email}).pipe(
      map(data => {
        if(data)
          this.allRecipes = data.map((r: any) => new Recipe(r));
        return this.allRecipes!;
      })
    );
  }


  addRecipe(recipe: Recipe): Observable<boolean> {
    return this.martha.insert('recipes-create', recipe).pipe(
      map(result => {
        if(result?.success)
          return true;
        else
          return false;
      })
    )
  }

  editRecipe(recipe: Recipe): Observable<boolean> {
    return this.martha.insert('recipes-update', recipe).pipe(
      map(result => {
        if(result?.success)
          return true;
        else
          return false;
      })
    )
  }

  delete(recipe: Recipe): Observable<any> {
    return this.martha.insert('recipes-delete', recipe).pipe(
      map(result => {
        if(result?.success)
          return true;
        else
          return false;
      })
    )
  }
}
