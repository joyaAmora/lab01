import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserCredentials } from '../models/user-credentials.model';
import { User } from '../models/user.model';
import { MarthaRequestService } from './martha-request.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly CURRENT_USER_KEY = 'recipeasy.currentUser';
  private readonly USERS_KEY = 'recipeasy.users';

  private _currentUser : User | null = null;

  get currentUser(): User | null {
    return this._currentUser;
  }

  get isLoggedIn(): boolean {
    return !!this._currentUser;
  }

  get users() {
    return this.usersCredentials.map(userCredentials => new User(userCredentials));
  }

  private get usersCredentials(): UserCredentials[] {
    let usersCredentials : UserCredentials[] = [];
    const storedUsers = JSON.parse(localStorage.getItem(this.USERS_KEY) ?? 'null');

    if (storedUsers) {
      usersCredentials = (storedUsers as UserCredentials[]).map(obj =>
        new UserCredentials(obj)
      );
    }

    return usersCredentials;
  }

  constructor(private martha: MarthaRequestService) {
    const storedCurrentUser = JSON.parse(localStorage.getItem(this.CURRENT_USER_KEY) ?? 'null');

    if (storedCurrentUser) {
      this._currentUser = new User(storedCurrentUser);
    }
  }

  private setCurrentUser(user: User | null) {
    this._currentUser = user;
    localStorage.setItem(this.CURRENT_USER_KEY, JSON.stringify(user));
  }

  private emailExists(email: string): boolean {
    return !!this.usersCredentials.find(credentials =>
      credentials.email == email
      );
  }

  logIn(credentials: UserCredentials): Observable<boolean> {
    return this.martha.select('users-login', credentials).pipe(
      map(data => {
        console.log("Auth Service", data);

        if (data && data.length == 1) {
          this.setCurrentUser(new User(data[0]));
          return true;
        } else {
          return false;
        }
      })
    );
  }

  signUp(credentials: UserCredentials): Observable<boolean> {
    return this.martha.insert('users-signup', credentials).pipe(
      map(result => {
        console.log('Auth service', result);

        if(result?.success){
          this.setCurrentUser(new User(credentials));
          return true;
        }
        else
          return false;
      })
    );
  }

  logOut() {
    this.setCurrentUser(null);
  }
}
