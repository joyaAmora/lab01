import { Routes } from "@angular/router";
import { LoginComponent } from "./components/login/login.component";
import { PageNotFoundComponent } from "./components/page-not-found/page-not-found.component";
import { ProfileDetailsComponent } from "./components/profile/profile-details/profile-details.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { AddRecipeComponent } from "./components/recipes/add-recipe/add-recipe.component";
import { RecipesComponent } from "./components/recipes/recipes.component";
import { SignupComponent } from "./components/signup/signup.component";
import { AuthGuard } from "./guards/auth.guard";

export const routes: Routes =[
    { path: '', component: LoginComponent, canActivate: [AuthGuard], },
    { path: 'signup', component: SignupComponent, canActivate: [AuthGuard], },
    { path: '', canActivateChild: [AuthGuard], children: [
      { path: 'recipes', component: RecipesComponent, },
      { path: 'profile', component: ProfileComponent, },
      { path: 'profile/:email', component: ProfileDetailsComponent },
    ]},

    { path: 'addRecipe', component: AddRecipeComponent, },
    { path: 'editRecipe/:id', component: AddRecipeComponent, },
    { path: '**', component: PageNotFoundComponent, },
  ];
